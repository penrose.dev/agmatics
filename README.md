# AgMatics

## Database

> Neo4j - Community Edition

### Database Setup

`docker compose up`

[neo4j console](http://localhost:7474)

Initialize schema [neo4j/init.cypher](/neo4j/init.cypher)

```cypher
MATCH (n)-[r]->(m) RETURN n, r, m;
```
