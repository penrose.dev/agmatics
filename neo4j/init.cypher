
// Create nodes
CREATE (user:User {name: 'User'});
CREATE (agronomist:Agronomist {name: 'Agronomist'});
CREATE (router:Router {name: 'Router'});
CREATE (call_tools:Call_Tools {name: 'Call_Tools'});
CREATE (data_collector:Data_Collector {name: 'Data_Collector'});

// Create relationships
MATCH (user:User), (agronomist:Agronomist)
CREATE (user)-[:INTERACTS_WITH]->(agronomist);

MATCH (agronomist:Agronomist), (router:Router)
CREATE (agronomist)-[:MESSAGE]->(router);

MATCH (router:Router), (agronomist:Agronomist)
CREATE (router)-[:CONTINUE]->(agronomist);

MATCH (router:Router), (call_tools:Call_Tools)
CREATE (router)-[:USES]->(call_tools);

MATCH (data_collector:Data_Collector), (router:Router)
CREATE (data_collector)-[:MESSAGE]->(router);

MATCH (router:Router), (data_collector:Data_Collector)
CREATE (router)-[:CONTINUE]->(data_collector);

MATCH (call_tools:Call_Tools), (agronomist:Agronomist)
CREATE (call_tools)-[:SENDER]->(agronomist);

MATCH (call_tools:Call_Tools), (data_collector:Data_Collector)
CREATE (call_tools)-[:SENDER]->(data_collector);
