const neo4j = require('neo4j-driver');
const dotenv = require('dotenv');

// Load environment variables from .env file
dotenv.config();

// Neo4j setup
const uri = 'bolt://localhost:7687';
const user = 'neo4j';
const password = 'localhost'; // Replace with your Neo4j password

const driver = neo4j.driver(uri, neo4j.auth.basic(user, password));
const session = driver.session();

async function queryNeo4j() {
  try {
    const result = await session.run(
      'MATCH (n) RETURN n LIMIT 25'
    );
    result.records.forEach(record => {
      console.log(record.get(0));
    });
  } finally {
    await session.close();
  }

  // on application exit:
  await driver.close();
}

async function main() {
  await queryNeo4j();
  // Add your LangChain interactions here
  // e.g., interacting with LangChain API or using @langchain packages
}

main().catch(console.error);
