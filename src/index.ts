import dotenv from 'dotenv';
dotenv.config();

import { EventEmitter } from 'eventemitter3';
import neo4j, { Driver, Session } from 'neo4j-driver';
import { DynamicStructuredTool, DynamicTool } from "@langchain/core/tools";
import { ChatOpenAI } from "@langchain/openai";
import { z } from "zod";

// Neo4j setup
const uri = 'bolt://localhost:7687';
const user = 'neo4j';
const password = 'localhost'; // Replace with your Neo4j password
const eventEmitter = new EventEmitter();
const driver: Driver = neo4j.driver(uri, neo4j.auth.basic(user, password));

const llm = new ChatOpenAI({
  model: "gpt-4",
  temperature: 0
});

const calculatorSchema = z.object({
  operation: z
    .enum(["add", "subtract", "multiply", "divide"])
    .describe("The type of operation to execute."),
  number1: z.number().describe("The first number to operate on."),
  number2: z.number().describe("The second number to operate on."),
});

const calculatorTool = new DynamicStructuredTool({
  name: "calculator",
  description: "Can perform mathematical operations.",
  schema: calculatorSchema,
  func: async ({ operation, number1, number2 }) => {
    // Functions must return strings
    if (operation === "add") {
      return `${number1 + number2}`;
    } else if (operation === "subtract") {
      return `${number1 - number2}`;
    } else if (operation === "multiply") {
      return `${number1 * number2}`;
    } else if (operation === "divide") {
      return `${number1 / number2}`;
    } else {
      throw new Error("Invalid operation.");
    }
  },
});

async function main() {
  // Define the model configuration
  const modelConfig = { model: "gpt-4" };
  // Create an instance of ChatOpenAI with the model configuration
  const model = new ChatOpenAI(modelConfig);
  // Assume calculatorTool is defined and imported properly
  // const calculatorTool: DynamicStructuredTool = { /* tool definition here */ };
  const llmWithTools = model.bindTools([calculatorTool]);
  const res = await llmWithTools.invoke("Calculate: multiply 3 by 12");

  console.log(res.tool_calls);
	console.log(res.content);

	if (res.tool_calls && res.tool_calls.length > 0) {
    for (const call of res.tool_calls) {
      if (call.name === 'calculator') {
        const result = await calculatorTool.func(call.args);
        console.log(`Calculation result for ${call.args.operation} ${call.args.number1} and ${call.args.number2}:`, result);
      }
    }
  }

}

// Execute the async function
main().catch(console.error);
